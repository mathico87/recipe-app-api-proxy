# recipe-app-api-proxy

Nginx proxy app for our recipe app API

## Usage

### Environment variables

* `LISTEN_PORT` -port to listen on (default:`8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default:`APP`)
* `APP_PORT` - port of the app to forward request to (default:`9000`)